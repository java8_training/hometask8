public class Product {
    String productName, productCategory, productGrade;
    double productPrice;
    int countOfProducts;

    Product(String productName, double productPrice, String productCategory, String productGrade, int countOfProducts){
        this.productName = productName;
        this.productPrice = productPrice;
        this.productCategory = productCategory;
        this.productGrade = productGrade;
        this.countOfProducts = countOfProducts;
    }

    @Override
    public String toString(){
        return "Product Name : "+productName+"\tProduct Price : "+productPrice+"\tProduct Category : "+productCategory+"\tProduct Grade : "+productGrade+"\tCount of products : "+countOfProducts;
    }
}
