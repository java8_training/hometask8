import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Main {
    static List<Product> productsList = new ArrayList<>();

    public static void main(String[] args) {
        CommonFunctions commonFunctions = new CommonFunctions();
        productsList = commonFunctions.setProductsList();

        Predicate<Product> priceCheck = (product) -> product.productPrice > 1000;
        Predicate<Product> categoryCheck = (product) -> product.productCategory.equalsIgnoreCase("Electronics");
        Predicate<Product> priceAndCategoryCheck = priceCheck.and(categoryCheck);

        //list of the products get all the products with price > 1000/-
        System.out.println("Products price > 1000");
        productsList.stream().filter(priceCheck).forEach((product -> System.out.println(product.toString())));

        //list of the products get all the products from electronics category.
        System.out.println("Products from electronics category");
        productsList.stream().filter(categoryCheck).forEach((product -> System.out.println(product.toString())));

        //list of the products get all the products with price> 1000/- and from electronics category
        System.out.println("Products from electronics category and price > 1000");
        productsList.stream().filter(priceAndCategoryCheck).forEach((product -> {
            product.productName = product.productName.toUpperCase();
            System.out.println(product);
        }));



    }
}
