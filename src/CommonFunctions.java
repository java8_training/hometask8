import java.util.ArrayList;
import java.util.List;

public class CommonFunctions {
    public List<Product> setProductsList() {
        List<Product> productsList = new ArrayList<>();
        productsList.add(new Product("PS5", 40000, "Electronics", "New",3));
        productsList.add(new Product("XBox Series S", 35000, "Electronics", "Used",1));
        productsList.add(new Product("iPhone", 80000, "Mobile", "New",5));
        productsList.add(new Product("Lucky Luke", 100, "Comics", "New",6));
        productsList.add(new Product("Charger", 50, "Electronics", "New",5));
        productsList.add(new Product("Alexa", 1000, "Electronics", "New",12));
        return productsList;
    }
}
